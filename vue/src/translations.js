
export function translatePath(path, lang = 'en') {
    if(lang == 'undefined'){
    	lang = 'en';
    }
	
    let pathTranslations = {
        en: {
            home    : '/wms/scannerApp/',
            user    : '/wms/scannerApp/user',
            test    : '/wms/scannerApp/test',
            items   : '/wms/scannerApp/items',
            scanproducts : '/wms/scannerApp/inventory/scanproducts',
        }
    };

    if (Object.prototype.hasOwnProperty.call(pathTranslations[lang], path)){
        return pathTranslations[lang][path];    
    }
    return '/';
}

export function convertStringsToVariables(str, variables) {
    let newstr = '';
    let res = str.split("%s");

    res.forEach(function(e,i){
        if(variables.length > i){
            newstr = newstr + e + variables[i];
        }
        else{
            newstr = newstr + e;    
        }
    });

    return newstr;
}



export function translateStrings(lang = 'en') {
    if(lang == 'undefined'){
    	lang = 'en';
    }

    let pathTranslations = {
        en: {
            test:'*Your subscribtion automatically renews 1. %s %s at the annual price of %s',
        },
        da: {
            test: '',
        }
    };

    return pathTranslations[lang];
}

